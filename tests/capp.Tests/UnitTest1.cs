using System;
using Xunit;
using tempconsole;

namespace capp.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Get1_ShouldReturn_1()
        {
            var oneResult = Program.Get1();
            Assert.True(oneResult == 1, "Expected 1");
        }

        [Fact]
        public void Get0_ShouldReturn_0()
        {
            var zeroResult = Program.Get0();
            Assert.True(zeroResult == 0, "Expected 0");
        }
    }
}
